<?php

namespace Nhattuanbl\LaraHelper\Notifications;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Slack\BlockKit\Blocks\ContextBlock;
use Illuminate\Notifications\Slack\BlockKit\Blocks\SectionBlock;
use Illuminate\Notifications\Slack\SlackMessage;
use Illuminate\Queue\Middleware\RateLimited;
use Nhattuanbl\LaraHelper\Helpers\IpHelper;
use Illuminate\Contracts\Queue\ShouldQueue;
use Throwable;

class ExceptionNotify extends Notification implements ShouldQueue
{
    use Queueable;

    const CONNECTION = 'redis';
    const QUEUE = 'default';
    const AVATAR = 'https://e7.pngegg.com/pngimages/35/219/png-clipart-computer-icons-exception-handling-symbol-caution-miscellaneous-cdr.png';

    private ?string $classname;
    private array $extra;
    private string $avatar = '';

    private int|string|null $eCode = null;
    private string $eClass;
    private string $eFile;
    private int $eLine;
    private string $eMess;
    private string $eTrace;

    public function __construct(public Throwable|Exception $exception, private string $channel, string|array|null $classOrExtra = null, private ?string $limiterName = null)
    {
        $this->classname = is_string($classOrExtra) ? $classOrExtra : null;
        $this->extra = is_array($classOrExtra) ? $classOrExtra : [];

        $this->eCode = @$exception->getCode();
        $this->eClass = get_class($exception);
        $this->eFile = $exception->getFile();
        $this->eLine = $exception->getLine();
        $this->eMess = $exception->getMessage();
        $this->eTrace = $exception->getTraceAsString();

        $this->onConnection(app()->isProduction() ? self::CONNECTION: 'sync');
        $this->onQueue(self::QUEUE);
    }

    public function via(object $notifiable): array
    {
        return ['slack'];
    }

    public function toArray()
    {

    }

    public function toSlack(object $notifiable): SlackMessage
    {
        $slackMsg = (new SlackMessage)
            ->to($this->channel)
            ->username(\auth()->user()?->username ?? 'GUEST')
            ->image($this->avatar)
            ->unfurlLinks(false)
            ->sectionBlock(function (SectionBlock $block) {
                $block->text(strtoupper(\request()->getMethod()) . ' ' . \request()->fullUrl());
                $block->field("*Environment:*\n" . config('app.env'))->markdown();
                $block->field("*Exception class:*\n" . $this->eClass)->markdown();
                $block->field("*Exception code:*\n" . $this->eCode)->markdown();
                $block->field("*Client ip:*\n" . IpHelper::getUserIP())->markdown();
            })
        ;

        if (is_string($this->classname)) {
            $slackMsg->sectionBlock(function(SectionBlock $block) {
                $block->field("*Origin class:*\n" . $this->classname)->markdown();
            });
        } else if (!empty($this->extra)) {
            $slackMsg->sectionBlock(function(SectionBlock $block) {
                foreach ($this->extra as $key => $value) {
                    $block->field("*$key:*\n" . $value)->markdown();
                }
            });
        }

        $slackMsg->contextBlock(function (ContextBlock $block) {
            $block->text('In <phpstorm://open?url=file://' . str_replace(base_path(), '', $this->eFile) .
                '|'.$this->eFile.'> at line `' . $this->eLine . '`')
                ->markdown();
        });

        $slackMsg->contextBlock(function (ContextBlock $block) {
            $block->text('Exception message at line ' . $this->eLine . ': ' . $this->eMess)->markdown();
        });

        if (\request()->isMethod('POST')) {
            $slackMsg->contextBlock(function (ContextBlock $block) {
                $block->text(json_encode(\request()->all(), JSON_PRETTY_PRINT));
            });
        }

        $slackMsg->dividerBlock();
        $slackMsg->contextBlock(function (ContextBlock $block) {
            $block->text("Stack Trace: \n" . strip_tags($this->eTrace))->markdown();
        });

        return $slackMsg;
    }

    public function shouldSend(object $notifiable, string $channel): bool
    {
        return !is_null($this->channel);
    }

    public function middleware(object $notifiable, string $channel): array
    {
        if (!$this->limiterName) {
            return [];
        }

        return [new RateLimited($this->limiterName)];
    }
}
