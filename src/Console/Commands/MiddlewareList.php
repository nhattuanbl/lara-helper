<?php

namespace Nhattuanbl\LaraHelper\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;
use Symfony\Component\Console\Command\Command as CommandAlias;

class MiddlewareList extends Command
{
    protected $signature = 'middleware:list {route_name? : The name of the route to show middleware for}';

    protected $description = 'Displays all global middleware or the middleware for a specific route';

    /**
     * @throws \ReflectionException
     */
    public function handle(): int
    {
        $router = app(Router::class);
        $kernel = app(Kernel::class);
        $routeName = $this->argument('route_name');

        if ($routeName) {
            $route = Route::getRoutes()->getByName($routeName);

            if (!$route) {
                $this->error("No route found with the name '{$routeName}'.");
                return CommandAlias::FAILURE;
            }

            $routeMiddleware = $route->gatherMiddleware();
            $resolvedMiddleware = [];
            foreach ($routeMiddleware as $middleware) {
                $resolvedMiddleware = array_merge(
                    $resolvedMiddleware,
                    $this->resolveMiddleware($middleware, $router)
                );
            }

            if (empty($resolvedMiddleware)) {
                $this->info("No middleware registered for route '{$routeName}'.");
            } else {
                $this->info("Middleware for route '{$routeName}':");
                foreach ($resolvedMiddleware as $middlewareClass) {
                    $this->line("- $middlewareClass");
                }
            }
        } else {
            $reflection = new \ReflectionClass($kernel);
            $property = $reflection->getProperty('middleware');
            $property->setAccessible(true);
            $middleware = $property->getValue($kernel);

            if (empty($middleware)) {
                $this->info('No global middleware registered.');
            } else {
                $this->info('Registered Global Middleware:');
                foreach ($middleware as $middlewareClass) {
                    $this->line("- $middlewareClass");
                }
            }
        }

        return CommandAlias::SUCCESS;
    }

    protected function resolveMiddleware($middleware, Router $router)
    {
        $resolved = [];
        if (array_key_exists($middleware, $router->getMiddlewareGroups())) {
            $groupMiddleware = $router->getMiddlewareGroups()[$middleware];
            foreach ($groupMiddleware as $groupMiddlewareItem) {
                $resolved = array_merge($resolved, $this->resolveMiddleware($groupMiddlewareItem, $router));
            }
        } elseif (array_key_exists($middleware, $router->getMiddleware())) {
            $resolved[] = $router->getMiddleware()[$middleware];
        } else {
            $resolved[] = $middleware;
        }

        return $resolved;
    }
}
