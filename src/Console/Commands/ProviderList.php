<?php

namespace Nhattuanbl\LaraHelper\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;

class ProviderList extends Command
{
    protected $signature = 'provider:list';

    protected $description = 'Displays loaded service providers';

    public function handle(): int
    {
        $app = app();
        $loadedProviders = $app->getLoadedProviders();
        $this->info('Loaded Service Providers:');

        $providers = array_keys($loadedProviders);
        foreach ($providers as $index => $providerClass) {
            $this->line(($index + 1) . ". $providerClass");
        }

        return CommandAlias::SUCCESS;
    }
}
