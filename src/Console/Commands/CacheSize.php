<?php

namespace Nhattuanbl\LaraHelper\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;

class CacheSize extends Command
{
    protected $signature = 'cache:size {driver?}';
    protected $description = 'Counts the total cache size.';

    public function handle()
    {
        $driver = $this->argument('driver') ?? config("cache.default");
        switch ($driver) {
            case 'file':
                $this->countFileSizeCache($driver);
                break;
            case 'redis':
                $this->countRedisSizeCache($driver);
                break;
            default:
                $this->info("Cache driver for store {$driver} does not support size calculation.");
                break;
        }
    }

    protected function countFileSizeCache($store): void
    {
        $path = config("cache.stores.{$store}.path");
        $totalSize = 0;

        foreach (Storage::allFiles($path) as $file) {
            $totalSize += Storage::size($file);
        }

        $totalSizeInMb = number_format($totalSize / 1024 / 1024, 2);
        $this->info("Total cache size for store '{$store}': {$totalSizeInMb} MB");
    }

    protected function countRedisSizeCache($store): void
    {
        try {
            $redis = Redis::connection('default');
        } catch (\Exception $e) {
            $this->error("Failed to connect to Redis store '{$store}': " . $e->getMessage());
            return;
        }

        $info = $redis->info('memory');
        if (!isset($info['used_memory'])) {
            $this->error("Memory usage information is unavailable for Redis store '{$store}'.");
            return;
        }

        $memoryUsage = $info['used_memory'];
        $memoryUsageInMb = number_format($memoryUsage / 1024 / 1024, 2);

        $this->info("Total memory usage for Redis store '{$store}': {$memoryUsageInMb} " . ($info['maxmemory'] > 0 ? "({$info['maxmemory_human']})" : 'MB'));
    }
}
