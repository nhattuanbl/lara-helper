<?php

namespace Nhattuanbl\LaraHelper\Helpers;

 class DBHelper
 {
     public static function isMongoConnection(string $connection): bool
     {
         return config('database.connections.' . $connection . '.driver') === 'mongodb';
     }
 }
