<?php

namespace Nhattuanbl\LaraHelper\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Query\Builder;

class SqlHelper
{
    /**
     * @param MorphToMany|MorphTo|Builder|\Illuminate\Database\Eloquent\Builder|HasMany|HasOne|BelongsTo|Model $builder
     */
    public static function debug($builder, bool $return = false)
    {
        var_dump(vsprintf(str_replace('?', '%s', $builder->toSql()), collect($builder->getBindings())
            ->map(fn($binding) => is_numeric($binding) ? $binding : "'{$binding}'")
            ->toArray()
        ));

        if ($return === false) {
            exit();
        }
    }
}
