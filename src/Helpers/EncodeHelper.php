<?php

namespace Nhattuanbl\LaraHelper\Helpers;

class EncodeHelper
{
    CONST BASE64_CHARS = '+/0123456789=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    CONST ENCODE_KEY = '*6FQk9h8_j$2DysXwGp3PdgW1xM4laINAtqRrJcOef7BYuH5UzToCZmnL0vbSiKVE';

    static function Base64Encrypt(string $str): string
    {
        return strtr(base64_encode($str), self::BASE64_CHARS, self::ENCODE_KEY);
    }

    static function Base64Decrypt(string $encoded): bool|string
    {
        return base64_decode(strtr($encoded, self::ENCODE_KEY, self::BASE64_CHARS));
    }

    static function OpenSSLEncrypt(string $str): string
    {
        return openssl_encrypt($str, 'AES-256-CBC', '111', 0, '1234567891011121');
    }

    static function OpenSSLDecrypt(string $encrypted): string
    {
        return openssl_decrypt($encrypted, 'AES-256-CBC', '111', 0, '1234567891011121');
    }

    static function SHAEncode(string $string, string $key = '111'): string
    {
        $key = sha1($key);
        $strLen = strlen($string);
        $keyLen = strlen($key);
        $hash= '';
        $j = 0;
        for ($i = 0; $i < $strLen; $i++) {
            $ordStr = ord(substr($string,$i,1));
            if ($j == $keyLen) { $j = 0; }
            $ordKey = ord(substr($key,$j,1));
            $j++;
            $hash .= strrev(base_convert(dechex($ordStr + $ordKey),16,36));
        }
        return $hash;
    }

    static function SHADecode(string $string, string $key = '111'): string
    {
        $key = sha1($key);
        $strLen = strlen($string);
        $keyLen = strlen($key);
        $hash = '';
        $j = 0;
        for ($i = 0; $i < $strLen; $i+=2) {
            $ordStr = hexdec(base_convert(strrev(substr($string,$i,2)),36,16));
            if ($j == $keyLen) { $j = 0; }
            $ordKey = ord(substr($key,$j,1));
            $j++;
            $hash .= chr($ordStr - $ordKey);
        }
        return $hash;
    }

}
