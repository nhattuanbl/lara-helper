<?php

namespace Nhattuanbl\LaraHelper\Helpers;

use Exception;
use Illuminate\Support\Facades\Config;

class ConfigHelper
{
    /**
     * @throws Exception
     */
    public static function MergeFromPath(string $path, string $key): void
    {
        if (!file_exists($path)) {
            throw new Exception("Config file not found: {$path}");
        }

        $config = include $path;
        self::setConfigValues($key, $config);
    }

    public static function MergeFromArr(array $config, string $key): void
    {
        self::setConfigValues($key, $config);
    }

    public static function MergeExistsArr(array|string $config, string $key): void
    {
        $items = is_string($config) ? include $config : $config;
        $existingConfig = Config::get($key);

        if (!is_array($existingConfig)) {
            throw new Exception("Config key does not exist: {$key}");
        }

        Config::set($key, array_merge($existingConfig, $items));
    }

    protected static function setConfigValues(string $baseKey, array $config): void
    {
        foreach ($config as $key => $value) {
            $fullKey = "{$baseKey}.{$key}";

            if (is_array($value) && empty($value)) {
                Config::set($fullKey, []);
            } elseif (is_array($value) && array_keys($value) !== range(0, count($value) - 1)) {
                self::setConfigValues($fullKey, $value);
            } else {
                Config::set($fullKey, $value);
            }
        }
    }
}
