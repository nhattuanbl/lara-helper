<?php

namespace Nhattuanbl\LaraHelper;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Nhattuanbl\LaraHelper\Console\Commands\CacheSize;
use Nhattuanbl\LaraHelper\Console\Commands\CheckExt;
use Nhattuanbl\LaraHelper\Console\Commands\DBCopy;
use Nhattuanbl\LaraHelper\Console\Commands\DBCreate;
use Nhattuanbl\LaraHelper\Console\Commands\ElasticPing;
use Nhattuanbl\LaraHelper\Console\Commands\MiddlewareList;
use Nhattuanbl\LaraHelper\Console\Commands\MongoPing;
use Nhattuanbl\LaraHelper\Console\Commands\MysqlPing;
use Nhattuanbl\LaraHelper\Console\Commands\PostgresPing;
use Nhattuanbl\LaraHelper\Console\Commands\ProviderList;
use Nhattuanbl\LaraHelper\Console\Commands\RedisPing;
use RandomLib\Factory as IrcmaxellRandom;
use RandomLib\Generator;
use Illuminate\Http\Request;

class LaraHelperProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(IrcmaxellRandom::class, function(Application $app): Generator {
            return (new IrcmaxellRandom)->getLowStrengthGenerator();
        });
        include_once __DIR__ . '/helper.php';
    }

    public function boot()
    {
        $this->commands([
            ElasticPing::class,
            MongoPing::class,
            MysqlPing::class,
            RedisPing::class,
            CheckExt::class,
            PostgresPing::class,
            DBCreate::class,
            DBCopy::class,
            CacheSize::class,
            MiddlewareList::class,
            ProviderList::class,
        ]);

        $this->extendRequest();
    }

    protected function extendRequest()
    {
        $topLevelDomains = ['com.vn', 'co.uk', 'org.uk', 'gov.uk', 'com.au', 'net.au', 'edu.au'];
        Request::macro('getDomain', function () use ($topLevelDomains) {
            $host = $this->getHost();
            $parts = explode('.', $host);
            $numParts = count($parts);

            if ($numParts < 2) {
                return $host;
            }

            $possibleTLD = implode('.', array_slice($parts, -2));
            $possibleFullTLD = implode('.', array_slice($parts, -3));

            if (in_array($possibleTLD, $topLevelDomains) && $numParts > 2) {
                return implode('.', array_slice($parts, -3));
            }

            return $possibleTLD;
        });

        Request::macro('getSubDomain', function () use ($topLevelDomains) {
            $host = $this->getHost();
            $parts = explode('.', $host);
            $numParts = count($parts);

            if ($numParts < 2) {
                return null;
            }

            $possibleTLD = implode('.', array_slice($parts, -2));
            if (in_array($possibleTLD, $topLevelDomains) && $numParts > 3) {
                return implode('.', array_slice($parts, 0, -3));
            } elseif ($numParts > 2) {
                return implode('.', array_slice($parts, 0, -2));
            }

            return null;
        });
    }
}
